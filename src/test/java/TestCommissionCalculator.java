import org.junit.Assert;
import org.junit.Test;


import java.math.BigDecimal;


public class TestCommissionCalculator {

    Commissions com = new Commissions();



    @Test
    public void testCommissionFor100Equals5(){

        BigDecimal sale = new BigDecimal("100");
        BigDecimal expectedResult = new BigDecimal("5.00");



        BigDecimal result = com.computeSaleCommission(sale);

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void testCommissionFor10001Equals600_06(){

        BigDecimal sale = new BigDecimal("10001");
        BigDecimal expectedResult = new BigDecimal("600.06");

        BigDecimal result = com.computeSaleCommission(sale);

        Assert.assertEquals(expectedResult, result);
    }


    @Test(expected = IllegalArgumentException.class)
    public void testCommissionForNegativeValues(){

        BigDecimal sale = new BigDecimal("-100");
        BigDecimal expectedResult = new BigDecimal("-5");

        BigDecimal result = com.computeSaleCommission(sale);

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void testDecimalInput(){

        BigDecimal sale = new BigDecimal("10000.8");
        BigDecimal expectedResult = new BigDecimal("600.04");

        BigDecimal result = com.computeSaleCommission(sale);

        Assert.assertEquals(expectedResult, result);
    }



}



// change to bigdecimal

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Commissions {

    public BigDecimal computeSaleCommission(BigDecimal sale) throws IllegalArgumentException {
        if (sale.compareTo(BigDecimal.ZERO) < 0 ) {
            throw new IllegalArgumentException();
        }

        if(sale.compareTo(new BigDecimal("10000")) > 0 ){
            return sale.multiply( new BigDecimal("0.06")).setScale(2, RoundingMode.FLOOR);
        } else {
            return sale.multiply( new BigDecimal("0.05"));
        }
    }

}